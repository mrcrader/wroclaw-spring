package com.ibagroup;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class TransactionController {

    @Autowired
    TransactionRepository transactionRepository;

    @Autowired
    AccountRepository accountRepository;

    @GetMapping("/")
    public String index() {
        return "Greetings from Spring Boot!";
    }

    @GetMapping("/transaction/retrieve")
    public List<Transactions> getAllTransactions() {
        return transactionRepository.findAll();
    }

    @GetMapping("/account/retrieve")
    public List<Accounts> getAllAccounts() {
        return accountRepository.findAll();
    }

    @PostMapping("/transaction/create")
    public void createTransaction(@RequestBody Transactions transactions) {
        if (transactions.getType().equals("CREDIT"))
            transactions.setAmount(transactions.getAmount() * -1);
        transactionRepository.save(transactions);
        Optional<Accounts> accounts;
        accounts = (accountRepository.findById(transactions.getAccounts().getId()));
        accounts.get().setBalance(accounts.get().getBalance() + transactions.getAmount());
        accountRepository.save(accounts.get());
    }

    @PostMapping("/account/create")
    public Accounts createAccount(@RequestBody Accounts accounts) {
        return accountRepository.save(accounts);
    }

    @GetMapping("/transaction/retrieveTransactions/{id}")
    public List<Transactions> retrieveTransactionsForAccounts(@PathVariable(value = "id") Long id){
        return transactionRepository.retrieveTransactionsForAccounts(id);
    }

    @GetMapping("/account/{id}")
    public Optional<Accounts> retrieveAccountById(@PathVariable(value = "id") Long id){
        return accountRepository.findById(id);
    }

}
