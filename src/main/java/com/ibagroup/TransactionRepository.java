package com.ibagroup;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface TransactionRepository extends JpaRepository<Transactions, Long> {
    @Query(value = "SELECT * FROM transactions t WHERE t.ACCOUNTS_ID = :id" , nativeQuery = true)
    List<Transactions> retrieveTransactionsForAccounts(@Param("id") Long id);
}
