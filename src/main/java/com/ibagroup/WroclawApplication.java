package com.ibagroup;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WroclawApplication {

    public static void main(String[] args) {
        SpringApplication.run(WroclawApplication.class, args);
    }

}
